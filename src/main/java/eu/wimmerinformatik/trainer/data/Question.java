/*
 *  Copyright 2014-2020 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wimmerinformatik.trainer.data;

import java.util.Date;
import java.util.List;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;

/**
 * POJO that holds a question and the answers and the current practice state for
 * it.
 *
 * TODO: Separate the question/answer from the practice state. We wouldn't need
 * the question and answer in the database, but could keep them in resources.
 * Also instead of keeping a level, I have to think if I store the history of
 * good and bad answers. This would be useful to build the planned
 * syncronisation with the web trainer.
 */
@Builder(toBuilder = true)
@Value
public class Question {
    /**
     * ID of the question.
     *
     * This is the same ID as on the web trainer.
     */
    private int id;

    /**
     * This is the grouping of questions to a handful of topics, that we
     * present for selection on the start screen.
     */
    private int topicId;

    /**
     * Textual reference in the questionaire.
     */
    private String reference;

    /**
     * Text of the question.
     */
    private String questionText;

    /**
     * Possible answers.
     *
     * The first one is the correct answer.
     */
    @Singular
    private List<String> answers;

    /**
     * How often has the question been answered correctly.
     *
     * = (correct answers) - (incorrect answers)
     */
    private int level;

    /**
     * When will we ask the question the next time.
     */
    private Date nextTime;
}
