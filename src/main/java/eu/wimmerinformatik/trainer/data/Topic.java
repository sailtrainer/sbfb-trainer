/*
 *  Copyright 2014-2020 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wimmerinformatik.trainer.data;

import lombok.Builder;
import lombok.Value;

/**
 * POJO that contains a grouping of questsions.
 *
 * We should the different topics we have questions for on the start screen of
 * the app.
 */
@Builder(toBuilder = true)
@Value
public class Topic {
    /**
     * ID of the topic.
     */
    private int id;

    /**
     * Ordering when showing the topics.
     *
     * Show {@link Topic} with smaller index before others with bigger index.
     */
    private int index;

    /**
     * How to name the topic.
     */
    private String name;
}
