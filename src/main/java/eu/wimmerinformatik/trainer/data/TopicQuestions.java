/*
 *  Copyright 2014-2020 Matthias Wimmer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wimmerinformatik.trainer.data;

import java.util.List;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class TopicQuestions {
    private Topic meta;

    @Singular
    private List<Question> questions;
}
